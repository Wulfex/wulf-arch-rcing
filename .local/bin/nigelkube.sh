#!/usr/bin/bash
pushd . >/dev/null 2>&1

echo "🐧 Starting Nigelkube..."

cd $HOME/nigel/local-k8s >/dev/null 2>&1

bash start-minikube.sh >/dev/null 2>&1
wait
nohup bash minikube dashboard >/dev/null 2>&1 &

echo "🍔 ~~~ Nigelkube started! ~~~ 🍔"

popd >/dev/null 2>&1
