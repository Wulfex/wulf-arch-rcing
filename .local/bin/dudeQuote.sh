#!/bin/bash
filename="${HOME}/.local/dudeDeChing.txt"

# Get the number of passages (each passage is separated by an empty line)
num_passages=$(grep -c '^$' "$filename")

# Generate a random number between 1 and the number of passages
random_passage=$((RANDOM % num_passages + 1))

# Use awk to print the random passage
awk -v num="$random_passage" 'BEGIN{RS="";FS="\n"} NR==num' "$filename" | cowsay -f bowlingball
