#!/bin/bash

# Remove all symbolic links

################################
# .config files
################################
# Alacritty
##
rm ~/.config/alacritty/alacritty.toml

##
# Bash
##
rm ~/.config/bash/.bash_profile
rm ~/.config/bash/.bashrc

##
# VSCode / COC Vetur
##
rm ~/.config/Code/User/snippets/vetur/snippets/typescript-class-component.vue

###
# Cowsay
###
rm -rf ~/.config/cowsay

##
# Fontconfig
##
rm ~/.config/fontconfig/conf.d/99-monofur-openmoji.conf
rm ~/.config/fontconfig/conf.d/99-ibm-3270-semi-condensed-symbola.conf
rm ~/.config/fontconfig/conf.d/99-noto-mono-symbola.conf
rm ~/.config/fontconfig/conf.d/99-comic-code-symbols.conf
rm ~/.config/fontconfig/conf.d/99-fira-book.conf

###
# NCSpot
##
rm ~/.config/ncspot/config.toml

##
# NeoVIM
##
rm ~/.config/nvim/init.vim
rm ~/.config/nvim/coc-settings.json

##
# Tmux
##
rm ~/.config/tmux/tmux.conf

##
# Zellij
##
rm -rf ~/.config/zellij

###
# VIM
###
rm ~/.config/vim/.vimrc

###
# Zellij
###
rm ~/.config/zellij/zellij.kdl
rm ~/.config/zellij/layouts/mylayout.kdl

##
# Zsh
##
rm ~/.zshenv
rm ~/.config/zsh/.zshrc
rm ~/.config/zsh/.zlogout

##
# Misc
##
rm ~/.config/.Xresources
rm ~/.config/.aliasrc
rm ~/.config/.gitignore_global

################################
# .local files
################################

##
# bin files
##
# Only remove bin files from this repo... accidentally removed
# LazyDocker one time... no bueno.
#
rm ~/.local/bin/7zHere
rm ~/.local/bin/colorlist
rm ~/.local/bin/docker-compose
rm ~/.local/bin/gitDiffWrapper
rm ~/.local/bin/mktouch
rm ~/.local/bin/NESRip
rm ~/.local/bin/randoMouse
rm ~/.local/bin/UpdateTaoQuoteFile.sh
rm ~/.local/bin/nigelkube.sh
rm ~/.local/bin/dudeQuote.sh
rm ~/.local/dudeDeChing.tx
