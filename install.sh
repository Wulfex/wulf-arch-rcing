#!/bin/bash

# Symbolically link config files so that changes will update in this repo

################################
# .config files
################################
##
# Alacritty
##
mkdir -p ~/.config/alacritty
ln -s "${PWD}/.config/alacritty/alacritty.toml" ~/.config/alacritty/alacritty.toml

###
# Bash
###
mkdir -p ~/.config/bash
ln -s "${PWD}/.config/bash/.bash_profile" ~/.config/bash/.bash_profile
ln -s "${PWD}/.config/bash/.bashrc" ~/.config/bash/.bashrc

##
# VSCode / COC Vetur
##
mkdir -p ~/.config/Code/User/snippets/vetur/snippets
ln -s "${PWD}/.config/Code/User/snippets/vetur/snippets/typescript-class-component.vue" ~/.config/Code/User/snippets/vetur/snippets/typescript-class-component.vue

###
# Cowsay
###
mkdir -p ~/.config/cowsay/cows
ln -s "${PWD}/.config/cowsay/cows/bowlingball.cow" ~/.config/cowsay/cows/bowlingball.cow

##
# Fontconfig
##
mkdir -p ~/.config/fontconfig/conf.d
ln -s "${PWD}/.config/fontconfig/conf.d/99-monofur-openmoji.conf" ~/.config/fontconfig/conf.d/99-monofur-openmoji.conf
ln -s "${PWD}/.config/fontconfig/conf.d/99-ibm-3270-semi-condensed-symbola.conf" ~/.config/fontconfig/conf.d/99-ibm-3270-semi-condensed-symbola.conf
ln -s "${PWD}/.config/fontconfig/conf.d/99-noto-mono-symbola.conf" ~/.config/fontconfig/conf.d/99-noto-mono-symbola.conf
ln -s "${PWD}/.config/fontconfig/conf.d/99-comic-code-symbols.conf" ~/.config/fontconfig/conf.d/99-comic-code-symbols.conf
ln -s "${PWD}/.config/fontconfig/conf.d/99-fira-book.conf" ~/.config/fontconfig/conf.d/99-fira-book.conf

##
# NCSpot
##
mkdir -p ~/.config/ncspot
ln -s "${PWD}/.config/ncspot/config.toml" ~/.config/ncspot/config.toml

##
# NeoVIM
##
mkdir -p ~/.config/nvim
ln -s "${PWD}/.config/nvim/init.vim" ~/.config/nvim/init.vim
ln -s "${PWD}/.config/nvim/coc-settings.json" ~/.config/nvim/coc-settings.json

##
# Tmux
##
mkdir -p ~/.config/tmux
ln -s "${PWD}/.config/tmux/.tmux.conf" ~/.config/tmux/tmux.conf

###
# VIM
###
mkdir -p ~/.config/vim
ln -s "${PWD}/.config/vim/.vimrc" ~/.config/vim/.vimrc

##
# Zellij
##
mkdir -p ~/.config/zellij
mkdir -p ~/.config/zellij/layouts
ln -s "${PWD}/.config/zellij/config.kdl" ~/.config/zellij/config.kdl
ln -s "${PWD}/.config/zellij/layouts/mylayout.kdl" ~/.config/zellij/layouts/mylayout.kdl

##
# Zsh
##
# This one has to live in Home dir
ln -s "${PWD}/.config/zsh/.zshenv" ~/.zshenv
mkdir -p ~/.config/zsh
ln -s "${PWD}/.config/zsh/.zshrc" ~/.config/zsh/.zshrc
ln -s "${PWD}/.config/zsh/.zlogout" ~/.config/zsh/.zlogout
# Create the histfile
mkdir -p ~/.local/share/zsh
touch ~/.local/share/zsh/.histfile

##
# Misc
##
ln -s "${PWD}/.config/.Xresources" ~/.config/.Xresources
ln -s "${PWD}/.config/.aliasrc" ~/.config/.aliasrc
ln -s "${PWD}/.config/.gitignore_global" ~/.config/.gitignore_global

################################
# .local files
################################

##
# bin files
##
mkdir -p ~/.local/bin
ln -s "${PWD}/.local/bin/7zHere" ~/.local/bin/
ln -s "${PWD}/.local/bin/colorlist" ~/.local/bin/
ln -s "${PWD}/.local/bin/docker-compose" ~/.local/bin/
ln -s "${PWD}/.local/bin/gitDiffWrapper" ~/.local/bin/
ln -s "${PWD}/.local/bin/mktouch" ~/.local/bin/
ln -s "${PWD}/.local/bin/NESRip" ~/.local/bin/
ln -s "${PWD}/.local/bin/randoMouse" ~/.local/bin/
ln -s "${PWD}/.local/bin/UpdateTaoQuoteFile.sh" ~/.local/bin/
ln -s "${PWD}/.local/bin/nigelkube.sh" ~/.local/bin/
ln -s "${PWD}/.local/bin/dudeQuote.sh" ~/.local/bin/
ln -s "${PWD}/.local/dudeDeChing.txt" ~/.local/
