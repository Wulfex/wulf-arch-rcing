export ZDOTDIR="$HOME/.config/zsh"

export EDITOR=nvim
export TERMINAL=alacritty
# Add local bin
export PATH=$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.nvm:$PATH
#. "$HOME/.cargo/env"
export CR_PAT="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"
export DOTNET_WATCH_RESTART_ON_RUDE_EDIT=1
