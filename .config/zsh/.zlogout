if command -v zellij &> /dev/null; then
  # If zellij is installed, delete all sessions on logout
  zellij delete-all-sessions
fi
