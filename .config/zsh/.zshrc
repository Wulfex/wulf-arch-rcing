# Lines configured by zsh-newuser-install
HISTFILE=~/.local/share/zsh/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _approximate
zstyle :compinstall filename '/home/chadba-fedora/.config/zsh/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

PROMPT='%F{cyan}%B%3~%b%f %# '
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/.aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/.aliasrc"

if [ ! -x /usr/bin/curl ] ; then
  echo "Direnv not installed..."
else
  # add direnv hook
  eval "$(direnv hook zsh)"
fi

# XDG Exports
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export COWPATH="/usr/share/cowsay/cows"
# set COWPATH so it includes user's cows
if [ -d "$XDG_CONFIG_HOME/cowsay/cows" ] ; then
    COWPATH="$COWPATH:$XDG_CONFIG_HOME/cowsay/cows"
fi

 export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# place this after nvm initialization!
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc

# Check if in interactive mode
#if [[ $- == *i* ]]; then
#  # Check if in TMUX session
#  if [ "$TERM_PROGRAM" = tmux ]; then
#  else
#    /bin/zsh -c "curl wttr.in\?0"
#  fi
#fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# Gets the daily quote from the Tao Te Ching and pipes it to cowsay
#bat $HOME/.local/taoquote.txt | cowsay -f bowlingball

# Gets a random quote from the Dude De Ching, a Tao Te Ching parody, and pipes it to cowsay
bash $HOME/.local/bin/dudeQuote.sh
