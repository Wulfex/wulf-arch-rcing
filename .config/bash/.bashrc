#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Open terminals with tmux
if [[ $DISPLAY ]]; then
    # If not running interactively, do not do anything
    [[ $- != *i* ]] && return
    test -z "$TMUX" && (tmux attach || tmux new-session)
fi

#Load aliases
[ -f $HOME/.config/aliasrc ] && source $HOME/.config/aliasrc

