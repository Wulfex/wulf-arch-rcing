""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plug Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.config/nvim/plugged')

""""""""""""""""""""""""""""""""""""""""""""
" Themes
""""""""""""""""""""""""""""""""""""""""""""
" Top Picks
Plug 'sainnhe/everforest'
Plug 'jnurmine/Zenburn'
Plug 'vigoux/oak'
Plug 'NLKNguyen/papercolor-theme'
Plug 'foxbunny/vim-amber'
Plug 'ku-s-h/summerfruit256.vim'
Plug 'sainnhe/gruvbox-material'
" Probation
Plug 'edeneast/nightfox.nvim'
Plug 'ghifarit53/tokyonight-vim'
Plug 'rebelot/kanagawa.nvim'
Plug 'rose-pine/neovim'
" Subpar

""" Transparency Plugin
Plug 'xiyaowong/nvim-transparent'

""""""""""""""""""""""""""""""""""""""""""""
" Navigation/etc
""""""""""""""""""""""""""""""""""""""""""""
Plug 'scrooloose/nerdtree'
Plug 'bling/vim-airline'
"Plug 'bling/vim-bufferline'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jeetsukumaran/vim-buffergator'

"Telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'branch': '0.1.x' }


""""""""""""""""""""""""""""""""""""""""""""
" Code Completetion, etc.
""""""""""""""""""""""""""""""""""""""""""""
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'tpope/vim-commentary'
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }
Plug 'github/copilot.vim'

""""""""""""""""""""""""""""""""""""""""""""
" Code Formatters
""""""""""""""""""""""""""""""""""""""""""""
"Plug 'maxmellon/vim-jsx-pretty'
"Plug 'prettier/vim-prettier'
Plug 'w0rp/ale'

""""""""""""""""""""""""""""""""""""""""""""
" Syntax highlighting, etc.
""""""""""""""""""""""""""""""""""""""""""""
" JS/Typescript
Plug 'leafgarland/typescript-vim'
Plug 'pangloss/vim-javascript'

" JS Frameworks
Plug 'posva/vim-vue'
"Plug 'evanleck/vim-svelte'
Plug 'leafOfTree/vim-svelte-plugin'

" Dotnet
Plug 'jlcrochet/vim-razor'
Plug 'OmniSharp/omnisharp-vim'

" Java / Kotlin
Plug 'udalov/kotlin-vim'

" Other
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }

""""""""""""""""""""""""""""""""""""""""""""
" Diff Tooling
""""""""""""""""""""""""""""""""""""""""""""
Plug 'tpope/vim-fugitive'

""""""""""""""""""""""""""""""""""""""""""""
" Fuzzy Search Plugins
""""""""""""""""""""""""""""""""""""""""""""
Plug 'dyng/ctrlsf.vim'
" Plug 'nvim-telescope/telescope.nvim'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" END OF PLUGINS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""
" Silver Searcher - Ag Config (For CtrlSF)
""""""""""""""""""""""""""""""""""""""""""""
if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  let g:ctrlsf_default_root = 'cwd'

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
endif

""""""""""""""""""""""""""""""""""""""""""""
" CtrlP Config
""""""""""""""""""""""""""""""""""""""""""""
  let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" Sets CtrlP to allways use current dir when searching
  let g:ctrlp_working_path_mode = 'wa'

" let g:ctrlp_custom_ignore = {
"   \ 'dir': '\v[\/](\.git|node_modules|out|www|build|dist)$'
"   \ }
"

""""""""""""""""""""""""""""""""""""""""""""
" ALE Config
""""""""""""""""""""""""""""""""""""""""""""
let g:ale_fixers = {
\  '*': ['remove_trailing_lines', 'trim_whitespace'],
\  'javascript': ['prettier', 'eslint'],
\  'typescript': ['prettier', 'eslint'],
\  'typescriptreact': ['prettier', 'eslint'],
\  'javascriptreact': ['prettier', 'eslint']
\ }

let g:ale_sign_error = '❌'
let g:ale_sign_warning = '⚠️'

let g:ale_fix_on_save = 1

" disable showing errors inline
let g:ale_echo_cursor = 0
let g:ale_hover_to_preview = 1
let g:ale_hover_to_floating_preview = 1

" disable ALE so messages don't clog screen
let g:ale_enabled = 0

let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#ale#error_symbol = '❌'
let g:airline#extensions#ale#warning_symbol = '⚠️'

""""""""""""""""""""""""""""""""""""""""""""
" Airline Config
""""""""""""""""""""""""""""""""""""""""""""
"let g:rigel_airline = 1
"let g:airline_theme = 'tokyonight'
let g:airline_powerline_fonts = 1

""""""""""""""""""""""""""""""""""""""""""""
" Prettier Config
""""""""""""""""""""""""""""""""""""""""""""
"let g:prettier#autoformat = 1
"let g:prettier#autoformat_require_pragma = 0
"let g:prettier#autoformat_config_present = 1
"let g:prettier#exec_cmd_async = 1
"command! -nargs=0 Prettier :CocCommand prettier.forceFormatDocument
"autocmd TextChanged,InsertLeave *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.svelte,*.yaml,*.html Prettier

""""""""""""""""""""""""""""""""""""""""""""
" Copilot Config
""""""""""""""""""""""""""""""""""""""""""""
" prevents copilot accept on tab
let g:copilot_no_tab_map = v:true

""""""""""""""""""""""""""""""""""""""""""""
" Custom Commands
""""""""""""""""""""""""""""""""""""""""""""
command Trim :%s/\s\+$//e
" command SpellCheck :setlocal spell spelllang=en_us
"
"clears all buffers (%bd kills all buffers, e# opens the last buffer)
command ClearBuffs :up | %bd | e#

""""""""""""""""""""""""""""""""""""""""""""
" Keymappings
""""""""""""""""""""""""""""""""""""""""""""
" Train me in the ways of Ctrl+[ for escape
:nnoremap <C-c> <Nop>
"""
" Custom
"""
nmap <silent> <Esc><Esc> :nohlsearch<CR>
nmap <leader>tt :TransparentToggle<CR>

"""
" CtrlSF
"""

" Quick way to type :CtrlSF
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>

"""
" Buffergator Config
"""

" Remap leader B to Buffergator toggle
nnoremap <leader>b :call BgTabCabinetToggle()<cr>

let g:bgTabCabinet_is_open = 1

" custom toggle function because the default one is broken
function! BgTabCabinetToggle()
    if g:bgTabCabinet_is_open
        BuffergatorOpen
        let g:bgTabCabinet_is_open = 0
    else
        BuffergatorClose
        let g:bgTabCabinet_is_open = 1
    endif
endfunction

" kills all buffers, then opens the last buffer
nnoremap <leader>cb :<c-u>up <bar> %bd <bar> e#<cr>

"""
" Copilot
"""
imap <silent><script><expr> <A-y> copilot#Accept("\<CR>")
imap <A-[> <Plug>(copilot-dismiss)

"""
" NERDTree Config
"""
nnoremap <C-n>t :NERDTreeToggle<CR>
nnoremap <C-n>f :NERDTreeFind<CR>

"""
" COC Config
"""

" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)


" These are largely unused, but I'm keeping them here for reference
" Show all diagnostics.
"nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
"nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
"nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
"nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
"nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

""""""""""""""""""""""""""""""""""""""""""""
" Vim Configs
""""""""""""""""""""""""""""""""""""""""""""
filetype plugin indent on
"set omnifunc=syntaxcomplete#Complete
set hidden
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set splitbelow splitright
set number relativenumber
set termguicolors
set bg=dark

colorscheme everforest
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:·
set list
set wildmenu
"set wildignore+=*/node_modules/*,*/platforms/*,*/platform-tools/*,*/www/*,*/plugins/*,*/selenium/*,*/docs/*,*/dist/*,*/out/*
set exrc
set nospell spelllang=en_us
set undodir=~/.local/share/nvim/undo
set undofile

"" netrw conigs
let g:netrw_winsize = 15
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1

""""""""""""""""""""""""""""""""""""""""""""
" COC Config
""""""""""""""""""""""""""""""""""""""""""""
let g:coc_node_path = '~/.nvm/versions/node/v21.7.1/bin/node'

""
" Enable typescript and vetur
""
"      \ 'coc-vetur',
"      \ 'coc-svelte',

let g:coc_global_extensions = [
      \ 'coc-tsserver',
      \ 'coc-spell-checker'
\ ]
